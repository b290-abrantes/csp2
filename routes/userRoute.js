const express = require("express");
const router = express.Router();

const auth = require("../auth")

const userController = require("../controllers/userController");

// Adding this route for additional purpose of checking duplicate emails
// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Router for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Router to get token
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Router for updating user to Admin
router.patch("/:userId/admin", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin){
		userController.updateUserAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" })
		res.send(false)
	}
});

// Route to create order/s
router.post("/checkout", auth.verify, (req, res) => {

	let newOrder = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
		// Product information
		// products : req.body.products,
		productId : req.body.productId,
		productName : req.body.productName,
		quantity : req.body.quantity,
		totalAmount : req.body.totalAmount
	}

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin === false) {
	userController.checkout(newOrder).then(resultFromController => res.send(resultFromController));
	} else {
		console.log("unauthorized user");
		res.send(false);
	}

});

// Route for retrieving user details
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can access user details
router.get("/userDetails", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getUser({ userId : userData.id }).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving user orders
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can access user details
router.get("/myOrders", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getUserOrders({ userId : userData.id }).then(resultFromController => res.send(resultFromController));

});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;