const express = require("express");
const router = express.Router();

const auth = require("../auth")

const productController = require("../controllers/productController");

// Route for creating a product
router.post("/add", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user"})
		res.send(false);

	}

});

// Route for getting all products
router.get("/all", auth.verify, (req, res) => { 

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user"})
		res.send(false);

	}
});

// Route for retrieving all ACTIVE products
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the products
router.get("/", (req, res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific/single product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:productId", (req, res) => {

	console.log(req.params);

	// Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/product/613e926a82198824c8c4ce0e
	// The product Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "productId" in the route
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
// JWT verification is needed for this route to ensure that a user is an Admin and logged in before updating a product
router.put("/:productId/update", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" })
		res.send(false)
	}
});

// Route to archiving a product
// A "PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" })
		res.send(false)
	}
});

// Route to activating a product
router.patch("/:productId/activate", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" })
		res.send(false)
	}
});


module.exports = router;