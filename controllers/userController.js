const bcrypt = require("bcrypt");

const auth = require("../auth");

const Product = require("../models/Product");

// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");

// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/
module.exports.checkEmailExists = (reqBody) => {

	return User.find({ email : reqBody.email }).then(result => {

		if(result.length > 0){

			return true;
		} else {

			return false;
		}
	}).catch(err => {

		console.log(err);

		return false;
	})
};

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = reqBody => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	// Save the created object to our database
	return newUser.save()
		.then(user => true)
			.catch(err => false);
};

// User authentication (login)
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = reqBody => {

	return User.findOne({ email : reqBody.email }).then(result =>{

		// User does not exist
		if(result == null){

			return false;

		// User exists
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// password matches
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result) }
			} else {

				return false;
			}
		}
	}).catch(err => {

		console.log(err);

		return false;
	})
};

// Updating user status to Admin
module.exports.updateUserAdmin = (reqParams, reqBody) => {

	let updateAdminField = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateAdminField)
			.then(user => true)
			.catch(err => {
				console.log(err);

				return false;
			});
};

// Add product to user/Create order
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the product ID to the user's orderedProduct array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.checkout = async (newOrder) => {

	console.log(newOrder);
	// Add the product ID in the orderedProduct array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the order method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(newOrder.userId).then(user => {

		// Adds the productId in the user's orderedProduct array
		user.orderedProduct.push({ products : [{productId : newOrder.productId, productName : newOrder.productName, quantity : newOrder.quantity}], totalAmount: newOrder.totalAmount });
		// user.orderedProduct.push({ products : newOrder.products, totalAmount: newOrder.totalAmount });

		// Saves the updated user information in the database
		return user.save().then(user => true)
				.catch(err => {

					console.log(err);

					return false
				})
	}).catch(err => {

		console.log(err);
		console.log("error in retrieving user");

		return false
	});

	// for (let i = 0; i < newOrder.products.length; i++) {}

	// Add the user ID in the userOrders array of the product
	// Using the "await" keyword will allow the order method to complete updating the product before returning a response back to the frontend
	let isProductUpdated = await Product.findById(newOrder.productId).then(product => {

		// console.log(newOrder.products[0].productId);

		// Adds the userId in the product's userOrders array
		product.userOrders.push({ userId : newOrder.userId });

		return product.save().then(product => true)
				.catch(err => {

					console.log(err)

					return false
				});
	}).catch(err => {

		console.log(err);
		console.log("error in retrieving product");

		return false
	});


	return (isUserUpdated && isProductUpdated) ? true : false;
};

// Retrieving user details
module.exports.getUser = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	}).catch(err => {

		console.log(err);

		return false
	});

};

// Retrieving User Orders
module.exports.getUserOrders = (data) => {

	return User.findById(data.userId).then(result => {

		// Returns the all ordered product information
		return result.orderedProduct;

	}).catch(err => {

		console.log(err);

		return false
	});

};
