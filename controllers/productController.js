const Product = require("../models/Product");

const auth = require("../auth");

// Create a new product
/*
	Steps:
	1. Create a new Product object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Product to the database
*/
module.exports.addProduct = reqBody => {

	let newProduct = new Product(
		{
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
		}
	);

	return newProduct.save()
		.then(product => {
			return true;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
}

// Retrieve all products
/*
	Steps:
	1. Retrieve all the products from the database
*/
module.exports.getAllProducts =() => {

	return Product.find({}).then(result => result)
		.catch(err => {

			console.log(err);

			return false;
		});
};

// Retrieve all ACTIVE products
/*
	Steps:
	1. Retrieve all the products from the database with the property of "isActive" to true
*/
module.exports.getAllActiveProducts = () => {

	return Product.find({ isActive : true }).then(result => result)
		.catch(err => {

			console.log(err);

			return false;
		})
};

// Retrieving a specific/single product
/*
	Steps:
	1. Retrieve the product that matches the product ID provided from the URL
*/
module.exports.getProduct = reqParams => {

	return Product.findById(reqParams.productId).then(result => result).catch(err => {

			console.log(err)

			return false;
		})
};

// Update a product
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name : reqBody. name,
		description : reqBody.description,
		price : reqBody.price
	};

	// Syntax
		// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
				.then(product => true)
					.catch(err => {
						console.log(err);

						return false;
					});
};

// Archiving a product
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
			.then(product => true)
			.catch(err => {
				console.log(err);

				return false;
			});
};

// Activating a product
module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
			.then(product => true)
			.catch(err => {
				console.log(err);

				return false;
			});
};

